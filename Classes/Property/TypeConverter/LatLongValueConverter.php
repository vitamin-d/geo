<?php
namespace VITD\Geo\Property\TypeConverter;

use DataValues\Geo\Values\LatLongValue;
use Geocoder\Exception\CollectionIsEmpty;
use Geocoder\Exception\NoResult;
use Geocoder\Exception\QuotaExceeded;
use TYPO3\CMS\Extbase\Error\Error;
use TYPO3\CMS\Extbase\Property\PropertyMappingConfigurationInterface;
use TYPO3\CMS\Extbase\Property\TypeConverter\AbstractTypeConverter;

class LatLongValueConverter extends AbstractTypeConverter
{
    /**
     * Source types this converter can convert
     *
     * @var array<string>
     */
    protected $sourceTypes = ['string'];

    /**
     * Target type this converter can convert to
     *
     * @var string
     */
    protected $targetType = LatLongValue::class;

    /**
     * Priority for this converter
     *
     * @var int
     */
    protected $priority = 10;

    /**
     * Geocoder
     *
     * The specific implementation is configured via Extension Manager
     *
     * @var \VITD\Geo\Service\GeocodeService
     * @inject
     */
    protected $geocodeService;


    /**
     * Check whether it can handle the given source data and the given target type.
     *
     * @param mixed $source the source data
     * @param string $targetType the type to convert to.
     * @return bool TRUE if this TypeConverter can convert from $source to $targetType, FALSE otherwise.
     */
    public function canConvertFrom($source, $targetType)
    {
        return is_string($source) && (mb_strlen($source) > 2);
    }


    /**
     * Convert from $source to a geographic location
     *
     * The return value can be one of three types:
     * - \DataValues\Geo\Values\LatLongValue described by $source. This is the normal case.
     * - null. Indicates that $source was empty or [not found/understood AND configured to silently fail]
     * - \TYPO3\CMS\Extbase\Error\Error If $source was empty or [not found/understood AND NOT configured to silently
     *   fail] or a unexpected runtime failure / development error happened
     *
     * @param mixed $source Text describing an address
     * @param string $targetType Unused, should always be DataValues\Geo\Values\LatLongValue
     * @param array $convertedChildProperties Unused
     * @param PropertyMappingConfigurationInterface $configuration Type converting configuration
     *
     * @return LatLongValue|null|Error The location described by $source, null, or an error object if a user-error occurred
     *
     * @throws \TYPO3\CMS\Extbase\Property\Exception\TypeConverterException thrown in case a developer error occurred
     */
    public function convertFrom(
        $source,
        $targetType,
        array $convertedChildProperties = [],
        PropertyMappingConfigurationInterface $configuration = null
    )
    {
        $value = null;

        try {
            $value = $this->geocodeService->geocode($source);

        } catch (CollectionIsEmpty $e) {
            if ($configuration->getConfigurationValue(self::class, 'required') === true) {
                $value = new Error($e->getMessage(), (int)$e->getCode() > 0 ? (int)$e->getCode() : 1472196608);
            }

        } catch (NoResult $e) {
            if ($configuration->getConfigurationValue(self::class, 'required') === true) {
                $value = new Error($e->getMessage(), (int)$e->getCode() > 0 ? (int)$e->getCode() : 1472196594);
            }

        } catch (QuotaExceeded $e) {

            $value = new Error($e->getMessage(), (int)$e->getCode() > 0 ? (int)$e->getCode() : 1471955340);
        }

        return $value;
    }
}
