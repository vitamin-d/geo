<?php
namespace VITD\Geo\Service;

use DataValues\Geo\Values\LatLongValue;
use Geocoder\Exception\CollectionIsEmpty;
use Geocoder\Exception\NoResult;
use Geocoder\Exception\QuotaExceeded;
use Geocoder\Provider\GoogleMaps;
use Geocoder\Provider\LocaleAwareProvider;
use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Cache\Frontend\FrontendInterface;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;

/**
 * Service to provide geopraphic functionality
 */
class GeocodeService implements SingletonInterface
{

    /**
     * Extension configuration
     *
     * @var array
     */
    protected $extensionConfiguration;

    /**
     * Cache frontend
     *
     * @var FrontendInterface
     */
    protected $cache;

    /**
     * Geocoder
     *
     * The specific implementation is configured via Extension Manager
     *
     * @var \Geocoder\Geocoder
     * @inject
     */
    protected $geocoder;

    /**
     * Extbase configuration manager
     *
     * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface
     * @inject
     */
    protected $configurationManager;





    // -------------------------- object lifecycle --------------------------
    /**
     * Create this object
     */
    public function __construct()
    {
        $this->cache = GeneralUtility::makeInstance(CacheManager::class)->getCache('geocoder');

        $this->extensionConfiguration = GeneralUtility::removeDotsFromTS(
            unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['geo'])
        );
    }


    /**
     * Initialize this object
     *
     * @return void
     */
    public function initializeObject()
    {
        if ($this->geocoder instanceof LocaleAwareProvider) {
            $this->geocoder->setLocale(
                $this->configurationManager
                    ->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT)
                    ['config']['sys_language_isocode']
            );
        }
        if ($this->geocoder instanceof GoogleMaps) {
        }
    }





    // --------------------------- public methods ---------------------------
    /**
     * Look up an addresses location via geocoding api
     *
     * The underlying geocoder api can be chosen and configured in the extension configuration (via extension manager).
     * Results are cached to minimize both execution time and api requests.
     *
     * @param string $query Query string, describing a location
     *
     * @return LatLongValue Probable location as specified by $query
     *
     * @throws CollectionIsEmpty If the geocoder did not return a location for $query
     * @throws NoResult If the geocoder could not find a location for $query
     * @throws QuotaExceeded If the geocoder api limit is reached
     */
    public function geocode($query)
    {
        $cacheIdentifier = $this->calculateCacheIdentifier($query);

        if ($this->cache->has($cacheIdentifier)) {

            $value = $this->cache->get($cacheIdentifier);

        } else {

            $value = null;

            try {
                $coordinates = $this->geocoder->geocode($query)->first()->getCoordinates();
                $value = new LatLongValue($coordinates->getLatitude(), $coordinates->getLongitude());

                $this->cache->set($cacheIdentifier, $value, ['address'], 0);

            } catch (CollectionIsEmpty $e) {
                $this->cache->set($cacheIdentifier, $value, ['address', 'noresult'], $this->extensionConfiguration['expireNegativeResults']);
                throw $e;

            } catch (NoResult $e) {
                $this->cache->set($cacheIdentifier, $value, ['address', 'noresult'], $this->extensionConfiguration['expireNegativeResults']);
                throw $e;

            } catch (QuotaExceeded $e) {
                // Note: should we remember and process quota related errors?
                //    -> Would avoid repeated hits on the api. For now: Just ask again and again.
                throw $e;

            }
        }

        return $value;
    }





    // ---------------------- internal helper methods -----------------------
    /**
     * Calculate a cache identifier for a specified value
     *
     * If the value is a string, it tries to keep it both human-readable (for debugging purposes) and collision-free
     * If the value is an array, its key ordering is normalized to avoid cache pollution.
     *
     * @param array|string $source Input value
     *
     * @return string Cache identifier that fits TYPO3's caching frameworks' rules
     */
    protected static function calculateCacheIdentifier($source)
    {
        if (is_array($source)) {
            ksort($source);

            return sha1(json_encode(array_unique($source)));
        }

        return sprintf('%s-%s', mb_strcut(preg_replace('/[^a-zA-Z0-9]+/', '_', $source), 0, 80), sha1($source));
    }
}
