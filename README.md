Geo
===

Functionality for geographic locations. Provides a geocoder service and a TypeConverter that
converts text addresses to geographic locations.

## Features
* Geocode Service
* Underlying geocoder selectable and configurable via Extension Manager configuration
  * Google Maps
  * Nominatim server
  * OpenStreetMap
* Result caching


## Caching

To save api requests and speed up repeated requests to the same address, all geocoder results are cached using the
highly configurable TYPO3 Caching Framework.


## Bugs

Despite being by far the most popular geocoder composer package, `willdurand/geocoder` is still somewhat cumbersome
to use. Specifically, it does not allow setting properties by other means than as constructor arguments which makes
it impossible to configure them at all using TYPO3 injection capabilities. Thus, most configuration options settable 
in Extension Manager **are unused** (*currently*).


## ToDo & Ideas

Repair `willdurand/geocoder`, then finish this extension.

Redesign to lift the limit of one single location per query, because geocoder services typically deliver multiple
probable results. This fact (and the reported reliabilty of each result) are at the moment completely hidden by the
service.
