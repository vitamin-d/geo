<?php
defined('TYPO3_MODE') or die();


call_user_func(function($packageKey) {
    if (!is_array($GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['geocoder'])) {
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['geocoder'] = [];
    }
    if (!isset($GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['geocoder']['backend'])) {
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['geocoder']['backend'] =
            \TYPO3\CMS\Core\Cache\Backend\Typo3DatabaseBackend::class;
    }

    $geocoder = array_key_exists($packageKey, $GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'])
        ? unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$packageKey])['geocoder']
        : \Geocoder\Provider\GoogleMaps::class; // this is the default in ext_conf_template.txt too

    TYPO3\CMS\Core\Utility\GeneralUtility
        ::makeInstance(\TYPO3\CMS\Extbase\Object\Container\Container::class)
        ->registerImplementation(\Geocoder\Geocoder::class, $geocoder);
    TYPO3\CMS\Core\Utility\GeneralUtility
        ::makeInstance(\TYPO3\CMS\Extbase\Object\Container\Container::class)
        ->registerImplementation(\Geocoder\Provider\Provider::class, $geocoder);
    TYPO3\CMS\Core\Utility\GeneralUtility
        ::makeInstance(\TYPO3\CMS\Extbase\Object\Container\Container::class)
        ->registerImplementation(\Geocoder\Provider\LocaleAwareProvider::class, $geocoder);
    TYPO3\CMS\Core\Utility\GeneralUtility
        ::makeInstance(\TYPO3\CMS\Extbase\Object\Container\Container::class)
        ->registerImplementation(
            \Ivory\HttpAdapter\HttpAdapterInterface::class,
            \Ivory\HttpAdapter\CurlHttpAdapter::class
        );


    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerTypeConverter(
        \VITD\Geo\Property\TypeConverter\LatLongValueConverter::class
    );
}, 'geo');
